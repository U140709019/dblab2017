#initial time 0.375sec/0.141sec
#after index 0.938sec
#after pk 0.016sec
use uniprot_example;
select * from Proteins;
load data local infile 'C:\\Users\\xxx\\Desktop\\insert.txt' into table Proteins fields terminated by '|';

select * 
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on proteins (uniprot_id);
drop index uniprot_index on proteins;

alter table Proteins add constraint pk_Proteins primary key (uniprot_id);