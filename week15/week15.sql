use movie_db;

alter table countries drop primary key;
alter table directors drop primary key;
alter table genres drop primary key;
alter table languages drop primary key;
alter table movie_directors drop primary key;
alter table movie_stars drop primary key;
alter table movies drop primary key;
alter table producer_countries drop primary key;
alter table stars drop primary key;


load data infile "C:\\Users\\csv\\movies.csv" into table movies fields terminated by ';';
load data infile "C:\\Users\\csv\\stars.csv" into table stars fields terminated by ';';
load data infile "C:\\Users\\csv\\directors.csv" into table directors fields terminated by ';';
load data infile "C:\\Users\\csv\\movie_directors.csv" into table movie_directors fields terminated by ';';
load data infile "C:\\Users\\csv\\genres.csv" into table genres fields terminated by ';';
load data infile "C:\\Users\\csv\\countries.csv" into table countries fields terminated by ';';
load data infile "C:\\Users\\csv\\languages.csv" into table languages fields terminated by ';';
load data infile "C:\\Users\\csv\\producer_countries.csv" into table producer_countries fields terminated by ';';
load data infile "C:\\Users\\csv\\movie_stars.csv" into table movie_stars fields terminated by ';';

alter table countries add primary key(country_id);
alter table directors add primary key(director_id);
alter table genres add primary key(movie_id,genres_name);
alter table languages add primary key(movie_id,language_name);
alter table movie_directors add primary key(movie_id,director_id);
alter table movie_stars add primary key(movie_id,star_id);
alter table movies add primary key(movie_id);
alter table producer_countries add primary key(producer_id,country_id);
alter table stars add primary key(star_id);

alter table directors add constraint fk_directors_countries foreign key (country_id) references countries(country_id);
alter table stars add constraint fk_stars_countries foreign key (country_id) references countries(country_id);
alter table languages add constraint fk_languages_movies foreign key (movie_id) references movies(movie_id);
alter table genres add constraint fk_genres_movies foreign key (movie_id) references movies(movie_id);
alter table movie_stars add constraint fk_movie_stars_movies foreign key (movie_id) references movies(movie_id);
alter table movie_stars add constraint fk_movie_stars_stars foreign key (star_id) references stars(star_id);
alter table movie_directors add constraint fk_movie_directors_movies foreign key (movie_id) references movies(movie_id);
alter table movie_directors add constraint fk_movie_directors_directors foreign key (director_id) references directors(director_id);
alter table producer_countries add constraint fk_producer_countries_movies foreign key (movie_id) references movies(movie_id);
alter table producer_countries add constraint fk_producer_countries_countries foreign key (movie_id) references movies(movie_id);

